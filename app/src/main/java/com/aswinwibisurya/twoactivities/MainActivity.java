package com.aswinwibisurya.twoactivities;

import android.app.Activity;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    EditText txtMessage;
    TextView tvReplyHeader;
    TextView tvReplyMessage;

    public static final int REQUEST_REPLY = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        txtMessage = findViewById(R.id.txtMessage);
        tvReplyHeader = findViewById(R.id.tvReplyHeader);
        tvReplyMessage = findViewById(R.id.tvReplyMessage);
    }

    public void sendMessage(View view) {
        String message = txtMessage.getText().toString();

        Intent intent = new Intent(this, SecondActivity.class);
        intent.putExtra("message", message);

        // not expecting result
//        startActivity(intent);

        startActivityForResult(intent, REQUEST_REPLY);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if(requestCode == REQUEST_REPLY) {
            if(resultCode == Activity.RESULT_OK) {
                String reply = data.getStringExtra("reply");
                tvReplyMessage.setText(reply);

                tvReplyHeader.setVisibility(View.VISIBLE);
                tvReplyMessage.setVisibility(View.VISIBLE);
            }
        }
    }
}
