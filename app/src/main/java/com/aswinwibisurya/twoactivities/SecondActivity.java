package com.aswinwibisurya.twoactivities;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class SecondActivity extends AppCompatActivity {

    TextView tvMessage;
    EditText txtReplyMessage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        tvMessage = findViewById(R.id.tvMessage);
        txtReplyMessage = findViewById(R.id.txtReplyMessage);

        Intent intent = getIntent();
        String message= intent.getStringExtra("message");

        tvMessage.setText(message);
    }

    public void sendReply(View view) {
        String reply = txtReplyMessage.getText().toString();
        Intent intent = new Intent();
        intent.putExtra("reply", reply);

        setResult(Activity.RESULT_OK, intent);

        finish();
    }
}
